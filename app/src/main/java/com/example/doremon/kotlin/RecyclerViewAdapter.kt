package com.example.doremon.kotlin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.doremon.kotlin.viewholder.ImageViewHolder
import com.example.doremon.kotlin.viewholder.TextViewHolder


class RecyclerViewAdapter(val list: ArrayList<DataModel>, val listener: onItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UpdateViewHolder).binView(list.get(position))
    }

    companion object {
        val TEXT: Int = 0;
        val IMAGE: Int = 1;
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val viewHolder: RecyclerView.ViewHolder = when (viewType) {
            TEXT -> TextViewHolder(listener, mInflater.inflate(R.layout.item_text, parent, false))
            else -> ImageViewHolder(listener, mInflater.inflate(R.layout.item_image, parent, false))
        }
        return viewHolder
    }

    override fun getItemViewType(position: Int): Int {
        val type = when (list.get(position).type) {
            DataModel.TEXT -> TEXT
            else -> IMAGE
        }
        return type
    }

    interface onItemClickListener {
        fun onTextClicked()
        fun onImageClicked()
    }

    interface UpdateViewHolder {
        fun binView(model: DataModel)
    }
}
