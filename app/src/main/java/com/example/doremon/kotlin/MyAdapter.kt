package com.example.doremon.kotlin

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.doremon.kotlin.viewholder.ImageViewHolder
import com.example.doremon.kotlin.viewholder.TextViewHolder

/**
 * Created by Doremon on 4/10/2018.
 */
class MyAdapter(diffCallback: DiffUtil.ItemCallback<DataModel>) : ListAdapter<DataModel, RecyclerView.ViewHolder>(diffCallback), RecyclerViewAdapter.onItemClickListener {
    override fun onTextClicked() {

    }

    override fun onImageClicked() {

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as RecyclerViewAdapter.UpdateViewHolder).binView(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val viewHolder: RecyclerView.ViewHolder = when (viewType) {
            RecyclerViewAdapter.TEXT -> TextViewHolder(this, mInflater.inflate(R.layout.item_text, parent, false))
            else -> ImageViewHolder(this, mInflater.inflate(R.layout.item_image, parent, false))
        }
        return viewHolder
    }
}