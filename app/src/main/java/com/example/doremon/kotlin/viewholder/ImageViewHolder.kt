package com.example.doremon.kotlin.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.example.doremon.kotlin.DataModel
import com.example.doremon.kotlin.R
import com.example.doremon.kotlin.RecyclerViewAdapter
import kotlinx.android.synthetic.main.item_image.view.*


class ImageViewHolder(listener: RecyclerViewAdapter.onItemClickListener, itemView: View) : RecyclerView.ViewHolder(itemView), RecyclerViewAdapter.UpdateViewHolder {
    val mImage: ImageView = itemView.findViewById(R.id.image_test)
    override fun binView(model: DataModel) {
        mImage.setImageResource(model.resource)
    }
}