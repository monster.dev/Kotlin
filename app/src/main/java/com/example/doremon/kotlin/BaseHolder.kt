package com.example.doremon.kotlin

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by Doremon on 4/6/2018.
 */
abstract class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
}