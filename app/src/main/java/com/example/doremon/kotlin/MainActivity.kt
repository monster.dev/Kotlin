package com.example.doremon.kotlin

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.io.File

class MainActivity : AppCompatActivity(), DemoInterface, RecyclerViewAdapter.onItemClickListener {
    override fun onTextClicked() {

    }

    override fun onImageClicked() {

    }

    override fun print() {
        println("hello kotlin")
    }

    // khai báo kiến
    val x: Int = 1  // final int x;
    val y = 1      // final int y =1;

    val boxedA: Int? = x
    val anotherBoxedA: Int? = x

    val x1: Int? = x

    var z: Int? = null
    var w = 2

    val name: String? = null  // final String name  = null;
    var lastName: String? = null

    var lengh: String? = "hello kotlin"

    //---------------------> String <--------------

    val fName = "Duc"
    val lName = "Nguyen"
    val tencuatoi = "my name is : $fName $lName"
    // cắt String
    val subName = "my name is : ${fName.substring(2)}"

    // suống dòng
    val suongdong = """
        |my
        |name
        |is
        |doremo""".trimMargin()

    //---------------------> Các loại toán tư <--------------
    //-----------> so sánh
    val sosanh = if (x > 3) "x>3" else "x<3"


    lateinit var mRecyclerView: RecyclerView
    lateinit var lisData: ArrayList<DataModel>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        mRecyclerView = findViewById(R.id.recycler_test)
//        btnAwaitNormal.setOnClickListener {
//            btnAwaitNormal.setText("aaaa")
//        }
//
//        //-----------> instanceof
////        if (x is Int) {
////            Log.e("Doremon : ", "Mày check đủng rồi đấy")
////        }
//
//        // -------- > ép kiểu
////        var epkieu = x as String
//
//
//        // -------- > switch
//
//        val mResult = when (x) {
//            1, 2 -> "1 or 2"
//            in 3..11 -> "3 đến 11"
//            else -> {
//                "xxx"
//            }
//        }
//
//        Log.e("Doremon : ", "mResult------> " + mResult)
//
//        // -------- > for
//        for (i in 1..11 step 2) { // for(int i = 1,i<11 ;i+=2)
//            Log.e("Doremon ", " i------------> " + i)
//        }
//
//        for (i in 1 until 11) {   // for(int i = 1,i<11 ;i++)
//            Log.e("Doremon ", " i1------------> " + i)
//        }
//
//
//        if (x in 1..10) {
//        }    // if(x >= 1 && x<= 10)
//
//        //--------------> Collections
//
//        var numbers = listOf(1, 2, 3)
//        Log.e("Doremon ", "Collections------->  " + numbers.get(1))
//
//        var map = mapOf(1 to "1", 2 to "2", 3 to "3")
//
//        Log.e("Doremon ", "Collections map------->  " + map.get(1))
//
//        var leg = lengh?.length   // check null
//
//        var leg1 = lengh!!.length // bắt buộc phải khác null nếu ko sẽ văng exection - > ok kotlin
//
//        Log.e("Doremon : ", "--------------> " + leg)
//
//        Log.e("Doremon :", "--------------> " + (boxedA === anotherBoxedA))
//
//        Log.e("Doremon :", "tao tên là  ----> " + tencuatoi)
//
//        Log.e("Doremon :", "sub name   ----> " + subName)
//
//        Log.e("Doremon :", "suong dòng   ----> " + suongdong)
//
//
//        //------>  cách khởi tạo hàm trong kotlin
//        Log.e("Doremon :", "sum-------------> " + sum(5))
//
//
////        val dataModel = DataModel("hello", "kotlin")
////
////        Log.e("Doremon : ", "dataMode-------------> " + dataModel.name1 + " " + dataModel.name2)
//
//        // add Data
//        var mData: DataModel
//        lisData = ArrayList<DataModel>()
//        for (i in 0 until 10) {
//            if (i % 2 == 0) {
//                mData = DataModel(i, DataModel.IMAGE, "", R.drawable.img_test)
//                lisData.add(mData)
//            } else {
//                mData = DataModel(i, DataModel.TEXT, "hello Bích Vân", 0)
//                lisData.add(mData)
//            }
//
//            Log.e("mRecyclerView : ", "-------> " + lisData.get(i).text)
//        }
//
//        // init recyclerview
//
//        mRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
////        val adapter = RecyclerViewAdapter(lisData, this)
////        mRecyclerView.adapter = adapter
//
//
//        ///////////////////////////////
//        val myadapter = MyAdapter(DIFF_CALLBACK)
//        mRecyclerView.adapter = myadapter
//
//        myadapter.submitList(lisData)
//
//
//        val deferred = (1..1_000_000).map { n ->
//            async {
//                n
//            }
//        }

//        Log.e("Doremon ", "deferred  " + deferred)
        aysncTest()
    }

    val DIFF_CALLBACK: DiffUtil.ItemCallback<DataModel> = object : DiffUtil.ItemCallback<DataModel>() {
        override fun areItemsTheSame(
                oldData: DataModel, newData: DataModel): Boolean {
            //  properties may have changed if reloaded from the DB, but ID is fixed
            return oldData.id == newData.id
        }

        override fun areContentsTheSame(
                oldData: DataModel, newData: DataModel): Boolean {
            // NOTE: if you use equals, your object must properly override Object#equals()
            // Incorrectly returning false here will result in too many animations.
            return oldData == newData
        }


    }


    //---------------> funtion
    fun helloKotlin(name: String? = "kotlin ngu lam") {
        println("Doremon , $name !")
    }

    fun isChoose(): Boolean {
        return true
    }

    fun sum(x: Int): Int = x * 2

    fun getFilePath(context: Context, path: String): File {
        val file = File("hello File in Kotlin");
        return file

    }

    fun initRecycler() {

    }

    private fun aysncTest() = async(UI) {
        val x = async(CommonPool) {
            for (i in 1..11) {
                Log.e("Doremon : ", "aysncTest " + i)
                Thread.sleep(1000)
                btnAwaitNormal.text = i.toString()
            }
        }
        x.await();
    }
}
