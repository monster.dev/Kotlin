package com.example.doremon.kotlin.viewholder

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.doremon.kotlin.DataModel
import com.example.doremon.kotlin.R
import com.example.doremon.kotlin.RecyclerViewAdapter

class TextViewHolder(litener: RecyclerViewAdapter.onItemClickListener, itemView: View) : RecyclerView.ViewHolder(itemView), RecyclerViewAdapter.UpdateViewHolder {
    val mTvtest: TextView = itemView.findViewById(R.id.tv_test)
    override fun binView(model: DataModel) {
        Log.e("binView ", "--------- > " + model.text)
        mTvtest.text = model.text
    }
}