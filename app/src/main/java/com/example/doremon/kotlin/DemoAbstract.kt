package com.example.doremon.kotlin

/**
 * Created by Doremon on 4/5/2018.
 */
abstract class DemoAbstract {
    abstract fun sume(): Int
}

class TestAstract : DemoAbstract() {
    object TestAstract {
        fun getInfo(): String {
            return "i am learning kotlin"
        }
    }

    override fun sume(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        TestAstract.getInfo()
    }

}
