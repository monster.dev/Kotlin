package com.example.doremon.kotlin

/**
 * Created by Doremon on 4/5/2018.
 */

open class DataModel(val id  :Int,val type: Int, val text: String, val resource: Int) {
    companion object {
        val TEXT = 0
        val IMAGE = 1
    }
}
